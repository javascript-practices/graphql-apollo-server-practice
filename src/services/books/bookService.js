const { ApolloServer, gql } = require('apollo-server');
const { buildFederatedSchema } = require("@apollo/federation");

const typeDefs = gql`
  type Book @key(fields: "id") {
    id: ID!
    title: String
    author: String
  }

  type Query {
    books: [Book]
  }
`;

const books = [
    {
      id: 1,
      title: 'Harry Potter and the Chamber of Secrets',
      author: 'J.K. Rowling',
    },
    {
      id: 2,
      title: 'Jurassic Park',
      author: 'Michael Crichton',
    },
  ];

const resolvers = {
    Query: {
      books: () => books,
    },
    Book: {
      __resolveReference(object) {
        return books.find(book => book.id === object.id);
      }
    }
  };

  const server = new ApolloServer({
    schema: buildFederatedSchema([
      {
        typeDefs,
        resolvers
      }
    ])
  });

server.listen({port : 4001}).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});