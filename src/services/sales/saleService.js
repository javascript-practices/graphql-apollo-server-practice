const { ApolloServer, gql } = require('apollo-server');
const { buildFederatedSchema } = require("@apollo/federation");

const typeDefs = gql`
  type Sale @key(fields: "id"){
    id: ID!
    book: Book
    quan: Int
  }

  extend type Book @key(fields: "id") {
    id: ID! @external
    
  }

  type Query {
    sales: [Sale]
  }
`;

const sales = [
    {
      id: "1",
      user: "1",
      book: { id: "1" },
      quan: 2
    },
    {
      id: "2",
      user: "1",
      book: { id: "2" },
      quan: 17
    },
    {
      id: "3",
      user: "2",
      book: { id: "1" },
      quan: 20
    },
    {
      id: "4",
      user: "2",
      book: { id: "1" },
      quan: 40
    }
]
const resolvers = {
    Query: {
      sales: () => sales,
    },

    
  };

const server = new ApolloServer({
    schema: buildFederatedSchema([
      {
        typeDefs,
        resolvers
      }
    ])
  });

server.listen({port : 4002}).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});