const { ApolloServer, gql } = require('apollo-server');
const { buildFederatedSchema } = require("@apollo/federation");

const typeDefs = gql`
  type User @key(fields: "id") {
    id: ID!
    name: String
  }

  type Query {
    users: [User]
  }
`;

const users = [
    {
        id: 1,
        name: 'Lucas',
    },
    {
        id: 2,
        name: 'Mati',
    },
    {
        id: 3,
        name: 'Leo',
    }
  ];

const resolvers = {
    Query: {
      users: () => users,
    },
  };

  const server = new ApolloServer({
    schema: buildFederatedSchema([
      {
        typeDefs,
        resolvers
      }
    ])
  });

server.listen({port : 4003}).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});